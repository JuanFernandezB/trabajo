
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Juan Fernandez
 */
public class Launcher {
    
    public Grafo grafoPesado;
    public Grafo grafoLivano;
    private Scanner sc;

    public Launcher() {
        this.grafoPesado = new Grafo();
        this.sc = new Scanner(System.in);
    }

    public void procesarDatos() {
        int opc = 0;

        do {
            imprimirMenu();
            System.out.println("Ingrese una opcion");
            opc = sc.nextInt();

            switch (opc) {
                case 1:
                    ingresaNumeroVertice();
                    break;
                case 2:
                    anexarArista();
                    break;
                case 3:
                    anexarPeso();
                    break;
                case 4:
                    imprimirGrafoLiviano();
                    break;
                case 5:
                    imprimirGrafoPesado();
                    break;
                case 6:
                    existeArista();
                    break;
                case 7:
                    obtenerPeso();
                    break;
                case 8:
                    borrarArista();
                    break;
                case 9:
                    liberarGrafo();
                    break;
                default: 
                    System.out.println("Opcion incorrecta");
                    
                   break;
            }
        } while (opc != 6);
    }

    public void imprimirMenu() {
        System.out.println("1. ingresar el numero de vertices");
        System.out.println("2. ingrese una Arista sin peso");
        System.out.println("3. ingrese una Arista con peso");
        System.out.println("4. imprimir Grafo Liviano");
        System.out.println("5. imprimir Grafo Pesado");
        System.out.println("6. verificar una arista");
        System.out.println("7. obtener peso de una Arista");
        System.out.println("8. Borrar Arista");
        System.out.println("9. Liberar Grafos");
        System.out.println("10. Verificar Adyacencia");
    }

    public void ingresaNumeroVertice() {
        int num;
        System.out.println("Ingrese el numero de vertices: ");
        num = sc.nextInt();
        grafoPesado = new Grafo(num);
        grafoLivano = new Grafo(num);
    }

    public void anexarArista() {
        int v1, v2;
        System.out.println("ingrese el vertice");
        System.out.print("v1: ");
        v1 = sc.nextInt();
        System.out.print("v2: ");
        v2 = sc.nextInt();
        grafoLivano.insertarArista(v1, v2);

    }

    public void anexarPeso() {
        int v1, v2;
        System.out.println("ingrese el vertice");
        System.out.print("v1: ");
        v1 = sc.nextInt();
        System.out.print("v2: ");
        v2 = sc.nextInt();
        System.out.println("Ingrese el peso de la arista");
        int peso = sc.nextInt();
        grafoPesado.insertarPesoArista(v1, v2, peso);
    }

    public void imprimirGrafoPesado() {
        grafoPesado.imprimirGrafo();
    }

    public void imprimirGrafoLiviano() {
        grafoLivano.imprimirGrafo();
    }

    public void existeArista() {
        int v1, v2;
        System.out.print("ingrese v1: ");
        v1 = sc.nextInt();
        System.out.print("ingrese v2: ");
        v2 = sc.nextInt();
        if (grafoPesado.existeArista(v1, v2)) {
            System.out.println("la Arista existe en el grafo Pesado");
        } else {
            System.out.println("La Arista NO existe en el grafo Pesado");
        }

        if (grafoLivano.existeArista(v1, v2)) {
            System.out.println("la Arista existe en el grafo Liviano");
        } else {
            System.out.println("La Arista NO existe en el grafo Liviano");
        }
    }

    public void obtenerPeso() {
        int v1, v2;
        System.out.print("ingrese v1: ");
        v1 = sc.nextInt();
        System.out.print("ingrese v2: ");
        v2 = sc.nextInt();

        System.out.println(grafoPesado.obtenerPeso(v1, v2));
    }

    public void borrarArista() {

        int v1, v2;
        System.out.print("ingrese v1: ");
        v1 = sc.nextInt();
        System.out.print("ingrese v2: ");
        v2 = sc.nextInt();

        grafoPesado.borrarArista(v1, v2);
        grafoLivano.borrarArista(v1, v2);
        System.out.println("Se ha borrado la Arista");
    }

    public void liberarGrafo() {
        grafoPesado.liberarGrafo();
        grafoLivano.liberarGrafo();
        System.out.println("Grafos son liberados");
    }

    public static void main(String[] args) {
        Launcher x = new Launcher();
        x.procesarDatos();
    }
    
}
