/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Juan Fernandez
 */
public class Grafo {
    
    private int numVertices;
    private int[][] grafo;

    public Grafo(int numVertices) {
        this.numVertices = numVertices;
        this.grafo = new int[numVertices][numVertices];

        for (int i = 0; i < grafo.length; i++) {
            for (int j = 0; j < grafo[i].length; j++) {
                grafo[i][j] = 0;
            }
        }

    }

    public Grafo() {
        this.numVertices = 5;
        this.grafo = new int[numVertices][numVertices];

        for (int i = 0; i < grafo.length; i++) {
            for (int j = 0; j < grafo[i].length; j++) {
                grafo[i][j] = 0;
            }
        }

    }

    public void insertarArista(int v1, int v2) throws ArrayIndexOutOfBoundsException {
        this.grafo[v1][v2] = 1;
    }
    
    public void insertarPesoArista(int v1, int v2, int peso) throws ArrayIndexOutOfBoundsException {
        this.grafo[v1][v2] = peso;
    }

    public boolean existeArista(int v1, int v2) throws ArrayIndexOutOfBoundsException {
        return (grafo[v1][v2] != 0);
    }

    public void borrarArista(int v1, int v2) throws ArrayIndexOutOfBoundsException {
        grafo[v1][v2] = 0;
        grafo[v2][v1] = 0;
    }
    
    public int obtenerPeso(int v1, int v2) throws ArrayIndexOutOfBoundsException{
        return grafo[v1][v2];
    }

    public void liberarGrafo(){
        for (int i = 0; i < grafo.length; i++) {
            for (int j = 0; j < grafo[i].length; j++) {
                grafo[i][j] = 0;
            }
        }
    }
    
    public void imprimirGrafo(){
        System.out.print("   ");
        for (int i = 0; i < grafo.length; i++) {
            System.out.print("     " + i);
        }
        System.out.println("");
        for (int i = 0; i < grafo.length; i++) {
            System.out.print("  " + i);
            for (int j = 0; j < grafo[i].length; j++) {
                System.out.print("     " + grafo[i][j]);
                
            }
            System.out.println("");
        }
    }
    
}
